---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.12
    jupytext_version: 1.7.1
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

+++ {"slideshow": {"slide_type": "slide"}}

# Gestion des devoirs informatiques avec GitLab

Nicolas M. Thiéry et al.

Laboratoire de Recherche en Informatique

Université Paris Sud/Saclay

Café LOOPS 8 et 15 décembre 2020

[Enregistrement](https://bbb.lri.fr/playback/presentation/2.0/playback.html?meetingId=39aeec07f03a12736c29bc54995f9e32240b5223-1608032032299)

+++ {"slideshow": {"slide_type": "notes"}}

## Résumé

Avec des collègues de Paris Sud et de Montréal, nous expérimentons
depuis quelques mois l'usage de nos GitLab's institutionnels (par ex.
gitlab.u-psud.fr) pour gérer les devoirs informatiques (TP, projet) de
nos étudiants. Comme dans les GitHub Classroom, l'idée est que
l'infrastructure fournie par une forge logicielle -- et les process
associés -- peuvent être détournés à relativement peu de frais et avec
beaucoup de souplesse pour toutes les étapes de la gestion des
devoirs : écriture collaborative, test et distribution du matériel
pédagogique, sauvegarde et transferts entre postes du travail des
étudiants, suivi de l'avancement, soumission, correction automatique
par intégration continue, commentaires, collecte des notes. Notamment
en exploitant l'API de la forge pour automatiser et donc simplifier
les opérations les plus courantes. Avec le bonus que l'on forme
progressivement les étudiants aux outils modernes de collaboration.

À ce stade, nous n'avons pas encore de solution complète clé en main,
mais nous avons accumulé exemples et bonnes pratiques. Mon objectif
pour cet exposé est d'inviter les collègues plus aventureux à venir
expérimenter avec nous pour leurs cours du deuxième semestre; et de
faire rêver les autres en attendant :-) Je raconterai notamment les
motivations (pourquoi pas Moodle? pourquoi une forge institutionnelle
plutôt que GitHub ?) et l'avancement de nos expérimentations (déployer
cela pour un cours de 200 étudiants de L1, ce n'était pas un peu
osé?). Un accent sera mis sur les interactions avec l'écosystème
Jupyter.

+++ {"slideshow": {"slide_type": "slide"}}

## Un retour d'expérience

+++ {"slideshow": {"slide_type": "fragment"}}

### À propos de quoi?

La gestion des devoirs «informatiques»:

+++ {"slideshow": {"slide_type": "fragment"}}

- TPs, projets, ...

+++ {"slideshow": {"slide_type": "fragment"}}

- Requérant l'utilisation de logiciels:
    - Programmation
    - Calcul scientifique (en maths, physique, ...)

+++ {"slideshow": {"slide_type": "fragment"}}

- Où le sujet et le rendu sont composés principalement de code:
  - Fichiers: C++, Python, ...
  - Feuilles de travail interactives: Jupyter, ... 
  - Typiquement:
    - de nombreux fichiers
    - des fichiers «à trous»
  - [Exemple de document interactif à trous](https://gitlab.u-psud.fr/Info111/Info111/blob/master/Semaine2/feuille1-valeurs-types-variables.ipynb)

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-warning">
À la recherche d'une meilleure terminologie!
</div>

+++ {"slideshow": {"slide_type": "subslide"}}

### Pour quoi?

+++ {"slideshow": {"slide_type": "subslide"}}

**Constat:** lourdeur de la gestion à la main!

+++ {"slideshow": {"slide_type": "fragment"}}

**Cadre:** explorer quels outils pourraient nous aider?

+++ {"slideshow": {"slide_type": "fragment"}}

**Objectifs:**

- Gagner du temps et de la qualité: automatisation
- Mieux suivre nos étudiants
- Faciliter l'hybridation

+++ {"slideshow": {"slide_type": "fragment"}}

**Stratégie:**

- Réutiliser le maximum d'infrastructure et de logiciels existants  
  Idéalement: juste une collection de bonnes pratiques

+++ {"slideshow": {"slide_type": "subslide"}}

### Sur quelles populations?

- Étudiants en L1 math-info, math-physique
    - Nombreux (200)
    - Raisonnablement à l'aise avec l'ordinateur
    - Zéro expérience en programmation, avec Linux, ...

+++ {"slideshow": {"slide_type": "fragment"}}

- Étudiants en master d'informatique
    - Petits effectifs
    - À l'aise avec les outils de développement collaboratifs

+++ {"slideshow": {"slide_type": "subslide"}}

### Avec qui?

Jérémie Neveu (LAL), Pierre-Thomas Froidevaux, Alexandre Blondin
Massé, Jean Privat (UQAM)

et pleins de collègues embarqués de force dans l'aventure :-)

+++ {"slideshow": {"slide_type": "subslide"}}

### DANGER, EXPÉRIENCE EN COURS !!!

- État actuel: prototypes, pour des cours spécifiques
- Janvier: solutions alpha à bidouiller pour les aventureux?
- Septembre: solutions un peu clé en main?

+++ {"slideshow": {"slide_type": "fragment"}}

Pourquoi en parler dès maintenant?
- Attirer des collaborateurs
- Urgence avec la gestion distanciel/présentiel/...

+++ {"slideshow": {"slide_type": "slide"}}

## Automatiser la gestion des devoirs?

![](gestion-des-devoirs.svg)

+++ {"slideshow": {"slide_type": "subslide"}}

### Besoins en infrastructure

#### Salles de TP virtuelles:

- [JupyterHub@Paris-Saclay](https://jupytercloud.lal.in2p3.fr/)
- [Binder](https://mybinder.org/)
- ...

+++ {"slideshow": {"slide_type": "fragment"}}

Pourrait faire l'objet d'une autre présentation

+++ {"slideshow": {"slide_type": "subslide"}}

#### Un dispositif d'échange ...

... pour les aller-retours entre enseignants et étudiants:

- Mail
- Clé USB
- Page web
- Dossier partagé
- ENT (par exemple: activité Devoir de eCampus)
- ???

+++ {"slideshow": {"slide_type": "subslide"}}

#### Spécifications

- Accessible de partout  
  Salle de TP, maison, ...
- Authentification et contrôle d'accès  
  Exemples:
  - aux documents instructeurs  
    typiquement: seuls les enseignants
  - à la soumission d'un étudiant  
    typiquement: seul l'étudiant et ses enseignants
- Protection des données personnelles
- Interface web intuitive
- Automatisable

+++ {"slideshow": {"slide_type": "subslide"}}

### Quelques candidats potentiels

+++ {"slideshow": {"slide_type": "subslide"}}

#### ENT (Moodle / eCampus)

- N'offre rien de particulier pour le code

+++ {"slideshow": {"slide_type": "subslide"}}

#### nbgrader

- Outil de gestion des devoirs
- Spécialisé documents Jupyter
- Correction semi-automatique avec UI efficace
- Difficulté technique: zone d'échange

+++ {"slideshow": {"slide_type": "subslide"}}

#### [CoCalc](cocalc.com) (Collaborative Calculation)

Infrastructure tout-en-un:
- Salle virtuelle
- Gestion des devoirs
- Intégration de nbgrader
- Logiciel libre, déployable sur site

Experts locaux: Samuel Lelièvre, Viviane Pons

Déploiement à Paris-Saclay?

+++ {"slideshow": {"slide_type": "slide"}}

## Hypothèse

+++ {"slideshow": {"slide_type": "fragment"}}

**Enseigner la programmation ou le calcul, c'est collaborer sur du code.**

+++ {"slideshow": {"slide_type": "fragment"}}

**Corollaire:** un outil de choix: les **forges logicielles**

+++ {"slideshow": {"slide_type": "fragment"}}

- Un espace de stockage partagé
- Avec authentification et gestion des droits
- Avec traçabilité forte (gestion de version: git)
- Conçu pour héberger du code
- Conçu pour la collaboration
- Conçu pour gérer des processus
- Très grande souplesse d'utilisation:
    - Interface web riche
    - Automatisation via API

+++ {"slideshow": {"slide_type": "subslide"}}

### Exemple: GitHub Classroom

- GitHub: forge logicielle
- GitHub Classroom:
    - Publication: un devoir = un dépôt git
    - Téléchargement: fork + clone
    - Soumission: pull-request
    - Correction automatique: intégration continue
    - + utilitaires de gestion de classe

+++ {"slideshow": {"slide_type": "subslide"}}

### Proposition: utiliser GitLab?

- GitLab: forge logicielle libre
- Possibilité de déploiement sur site:
    - Protection des données personnelles
    - Intégration dans le système d'information

+++ {"slideshow": {"slide_type": "fragment"}}

- À Paris-Saclay:
    - https://gitlab.u-psud.fr
    - Déployé par la DI depuis deux ans
    - Montée en puissance progressive:
        - en ressources
        - en fonctionnalités
    - Qualité de service: utilisable, mais sans garanties 7/7 24/24
    - Un grand merci à Marco Leoni pour la maintenance!

+++ {"slideshow": {"slide_type": "subslide"}}

### Est-ce raisonnable?

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-warning">
Pas de GitLab Classroom
</div>

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-success">
  Mais des collègues avec qui travailler!
</div>

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-warning">

Faire utiliser git à nos étudiants de L1 ?!?

</div>

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-warning">

Parfois encore plus ambitieux: aux collègues!

</div>

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-success">

Mitigation:<br>

<ul>
<li> Détourner l'interface web chaque fois que possible
<li> Identifier et automatiser les opérations essentielles
</ul>

</div>

+++ {"slideshow": {"slide_type": "slide"}}

## Démonstration

http://nicolas.thiery.name/Enseignement/Info111/

+++ {"slideshow": {"slide_type": "fragment"}}

### Point de vue étudiant

Interface réduite à deux commandes dans le shell:

    info-111 fetch <Devoir>

    info-111 submit <Devoir> <Groupe>

Détails:

http://nicolas.thiery.name/Enseignement/Info111/devoirs.html

+++ {"slideshow": {"slide_type": "subslide"}}

### Point de vue enseignant

https://gitlab.u-psud.fr/Info111/

+++ {"slideshow": {"slide_type": "subslide"}}

#### Tableau de bord du cours

![](tableau-de-bord.png)

+++ {"slideshow": {"slide_type": "subslide"}}

#### Synthèse soumissions

![](vue-soumissions-synthese.png)

+++ {"slideshow": {"slide_type": "subslide"}}

#### Soumissions des étudiants d'un groupe

![](vue-soumissions-groupe.png)

+++ {"slideshow": {"slide_type": "subslide"}}

#### Soumission = dépôt personnel d'un étudiant

![](depot-personnel.png)

+++ {"slideshow": {"slide_type": "fragment"}}

À noter:
- Le lien «Forked from» avec le dépôt sujet
- La pastille d'intégration continue
- Les dépôts personnels  plus récents sont privés

+++ {"slideshow": {"slide_type": "subslide"}}

### Résumé

![](gitlab.svg)

+++ {"slideshow": {"slide_type": "slide"}}

## Structure type d'un cours

+++ {"slideshow": {"slide_type": "subslide"}}

### Un cours = un groupe GitLab

+++ {"slideshow": {"slide_type": "fragment"}}

#### Info 111

- https://gitlab.u-psud.fr/Info111/
- Membres: responsable(s) d'UE

+++ {"slideshow": {"slide_type": "subslide"}}

- Dépôt instructeur:
    - https://gitlab.u-psud.fr/Info111/Info111
    - public ou privé???
    - Matériel pédagogique source:
        - Cours
        - Devoirs
        - Page web
    - Description de l'environnement logiciel:
        - https://gitlab.u-psud.fr/Info111/Info111/blob/master/binder/
        - liste des logiciels: environment.yml
        - scripts
    - Intégration continue:
        - https://gitlab.u-psud.fr/Info111/Info111/blob/master/.gitlab-ci.yml
        - Construction image docker
        - Validation des documents
        - TODO: Construction automatique page web
        - TODO: Construction automatique des dépôts sujets

+++ {"slideshow": {"slide_type": "subslide"}}

- Groupe annuel:
    - Membres: enseignants
    - https://gitlab.u-psud.fr/Info111/2020-2021
    - Un dépôt par devoir:
        - https://gitlab.u-psud.fr/Info111/2020-2021/Semaine4/
        - Dépôts étudiants = forks
            - https://gitlab.u-psud.fr/Info111/2020-2021/Semaine4/forks
    - Optionnel: un sous-groupe par groupe de TP
    - Optionnel: un dépôt pour des informations nominatives sensibles:
        - exemple: notes ou copies étudiants

+++ {"slideshow": {"slide_type": "fragment"}}

- Dépôt privé:
    - https://gitlab.u-psud.fr/Info111/Private
    - Sujets d'examen, ...
    - Forum discussions pédagogiques:
        - https://gitlab.u-psud.fr/Info111/Private/boards

+++ {"slideshow": {"slide_type": "subslide"}}

#### Quelques autres exemples

- M1 informatique: Combinatoire et algèbre  
  - https://gitlab.u-psud.fr/M1InfoMPRI/CombAlg
- M1 informatique: Algorithmique avancée  
  - https://gitlab.u-psud.fr/M1-ISD/AlgorithmiqueAvancee/
- L1-L3 physique: Méthodes numériques  
  - https://gitlab.u-psud.fr/MethNum/
- M1 informatique: TER  
  - https://gitlab.u-psud.fr/nicolas.thiery/ter-jupyter
  - http://nicolas.thiery.name/Enseignement/TER/presentation

+++ {"slideshow": {"slide_type": "slide"}}

## Expérimentations en cours

+++ {"slideshow": {"slide_type": "subslide"}}

### Correction par intégration continue

Exemple: https://gitlab.u-psud.fr/M1-ISD/AlgorithmiqueAvancee/Students/1-StructuresDeDonnees

+++ {"slideshow": {"slide_type": "fragment"}}

Bénéfices:
- Pour l'étudiant:
    - auto-évaluation immédiate, autant de fois qu'il le souhaite
    - vérification soumission
    - $\Rightarrow$ motivation

+++ {"slideshow": {"slide_type": "fragment"}}

- Pour l'équipe pédagogique:
    - code étudiant exécuté dans un environnement contrôlé
    - suivi de l'avancement
    - moins de correction à faire

+++ {"slideshow": {"slide_type": "fragment"}}

<div class="alert alert-warning">
Concevoir des tests pertinents et robustes et un barème est délicat.

Passer d'un indicateur à une métrique (note!) requiert quelques itérations.
</div>

+++ {"slideshow": {"slide_type": "subslide"}}

Quelques outils:
- pytest
- nbgrader

+++ {"slideshow": {"slide_type": "fragment"}}

À explorer:
-   Rendre plus robuste
    - Prérequis: montée en puissance des ressources de notre GitLab
-   Affichage synthétique riche
    - Piste: Intégration GitLab des rapports au format JUnit
    - Prérequis: montée en version de notre GitLab
-   Meilleure intégration avec nbgrader

+++ {"slideshow": {"slide_type": "subslide"}}

### Intégration avec nbgrader pour la correction manuelle

+++ {"slideshow": {"slide_type": "fragment"}}

Démo en local

+++ {"slideshow": {"slide_type": "fragment"}}

À faire / explorer:
- Collecte des devoirs depuis GitLab
- Correction collaborative
- Dépôt feedback après correction sur GitLab
- Où héberger les notes?
- Intégration eCampus?

+++ {"slideshow": {"slide_type": "subslide"}}

### Génération page web

- Exemple: http://nicolas.thiery.name/Enseignement/TER/presentation

+++ {"slideshow": {"slide_type": "fragment"}}

Outils:
- Jupyter Book: génération d'une page web interactive riche à partir
  de documents markdown, Jupyter, ...
- GitLab Pages: hébergement
- Intégration: déploiement automatique

+++ {"slideshow": {"slide_type": "fragment"}}

À faire / explorer:
- Préparer un template
- Déploiement GitLab Pages sur notre instance (fait: merci Marco!)
- Souci technique à régler

+++ {"slideshow": {"slide_type": "subslide"}}

### Communication avec les étudiants

+++ {"slideshow": {"slide_type": "fragment"}}

#### Forum d'aide

- Par tickets?

+++ {"slideshow": {"slide_type": "fragment"}}

#### Commentaires

- Tickets sur les dépôt des devoirs étudiants?

+++ {"slideshow": {"slide_type": "fragment"}}

#### Chat (type discord)

- Mattermost comme plugin GitLab

+++ {"slideshow": {"slide_type": "slide"}}

## Conclusion: utiliser une forge logicielle pour l'enseignement?

+++ {"slideshow": {"slide_type": "fragment"}}

- Une infrastructure pour (potentiellement) beaucoup de nos besoins
- Complémentarité avec JupyterHub / ENT / ...

+++ {"slideshow": {"slide_type": "fragment"}}

- Très flexible
- Adoption possible par touches successives
- Indépendance

+++ {"slideshow": {"slide_type": "fragment"}}

- Effet de bord: former étudiants et enseignants aux outils collaboratifs

+++ {"slideshow": {"slide_type": "subslide"}}

### Prérequis

- pour les étudiants: un peu de shell
- pour l'équipe pédagogique: un peu de git
- pour le responsable d'UE (ou gestionnaire): encore un peu lourd
- pour l'institution: déployer et faire monter en puissance un GitLab

+++ {"slideshow": {"slide_type": "subslide"}}

### Viabilité?

- 95%: trouver les bonnes pratiques:  
  Comment «détourner» cette infrastructure à notre profit
- 5%: ajouter un peu de poudre magique pour automatiser

+++ {"slideshow": {"slide_type": "fragment"}}

### Feuille de route
- Migration de [travo](https://gitlab.info.uqam.ca/info/travo/) vers Python: décembre
- Généralisation pour quelques autres cours: janvier
- Interopérabilité nbgrader pour correction manuelle: janvier
- Automatisation déploiement du cours: ?
- Exemple de cours (façon cookie-cutter): été prochain

+++ {"slideshow": {"slide_type": "fragment"}}

Si on s'y met à plusieurs, on ira vite :-)

Venez jouer avec nous!

(en attendant la mise en place d'un espace collaboratif, contacter Nicolas.Thiery@u-psud.fr)
