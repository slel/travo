# Contributing to Travo

All contributions and feedback are welcome!

## Developer workflow

Start contributing forking the Travo repository on gitlab.com
https://gitlab.com/travo-cr/travo, then clone your fork locally
```
$ git clone https://gitlab.com/<your_username>/travo
```

Inside the cloned repository you can build the project in
[editable mode](https://pip.pypa.io/en/stable/topics/local-project-installs/#editable-installs)
```
$ pip install -e .
```
To install all the dependencies needed for running the tests
```
$ pip install -e .[test]
```

Then tests can be run either typing `$ pyest` or `$ tox`.

## Contributing to the documentation

The Travo webpage and documentation are built using [sphinx](https://www.sphinx-doc.org/en/master/)
and the [pydata sphinx theme](https://pydata-sphinx-theme.readthedocs.io/en/stable/).

The sources of the documentation are in the `docs/sources/` repository and they are written in
[MyST markdown](https://mystmd.org/).

To build the documentation the needed dependencies can be installed using
```
$ pip install -e .[doc]
```
Then in the documentation directory
```
$ cd docs
```
the html version can be generated with
```
$ make html
```

The rendered documentation can be browsed in `build/html`.
